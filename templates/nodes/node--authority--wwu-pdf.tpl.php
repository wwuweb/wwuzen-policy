<?php

/**
 * @file
 * Template override for node type authority when viewed in PDF mode.
 */
print render($content['field_link']);
