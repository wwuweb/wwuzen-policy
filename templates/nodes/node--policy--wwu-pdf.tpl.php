<?php

/**
 * @file
 * Template override for policy content type viewed as pdf.
 */
?>
<div class="header-mask"></div>

<div class="header">

  <table class="header-layout-table">
    <tr class="header-layout-row">
      <td class="header-layout-cell" colspan="2">
        <?php print render($content['field_owner']); ?>
        <div class="clearfix-element"></div>
      </td>
    </tr>
    <tr class="header-layout-row">
      <td class="header-layout-cell" colspan="2">
        <h1>Policy</h1>
      </td>
    </tr>
    <tr class="header-layout-row">
      <td class="header-layout-cell">
        <?php print render($content['field_effective_date']); ?>
        <?php print render($content['field_revised']); ?>
        <?php print render($content['field_approved_by']); ?>
      </td>
      <td class="header-layout-cell">
        <?php print render($content['field_authority']); ?>
      </td>
    </tr>
  </table>

  <div class="clearfix-element"></div>

</div>

<div class="footer">
  <?php print render($title); ?>
</div>

<div class="sub-header">

  <?php if (isset($content['field_revises'])): ?>
  <?php print render($content['field_revises']); ?>
  <?php endif;?>

  <?php if (isset($content['field_replaces'])): ?>
  <?php print render($content['field_replaces']); ?>
  <?php endif;?>

  <?php if (isset($content['field_cancels'])): ?>
  <?php print render($content['field_cancels']); ?>
  <?php endif; ?>

  <?php if (isset($content['field_see_also'])): ?>
  <?php print render($content['field_see_also']); ?>
  <?php endif; ?>

</div>

<div class="content-header">
  <table>
    <tr>
      <td class="number-wrapper">
        <?php print render($content['field_number']); ?>
      </td>
      <td class="title-wrapper">
        <h2 class="title"><?php print render($content['field_policy_title']); ?></h2>
        <?php print render($content['field_application']); ?>
      </td>
    </tr>
  </table>
</div>

<div class="content">

  <?php if (isset($content['field_preamble'])): ?>
  <?php print render($content['field_preamble']); ?>
  <?php endif; ?>

  <?php if (isset($content['field_definitions'])): ?>
  <?php print render($content['field_definitions']); ?>
  <?php endif; ?>

  <?php print render($content['field_statements']); ?>

</div>

<div class="interior-header">
  <h1>Policy</h1>
</div>
