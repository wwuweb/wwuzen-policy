<?php

/**
 * @file
 * Template override for all fields on the policy content type.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
  <span class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:</span>
  <?php endif; ?>
  <?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
  <?php endforeach; ?>
</div>
