<?php

/**
 * @file
 * Template override for see also field.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <table>
    <tr>
      <td class="field-label"<?php print $title_attributes; ?>>
        <?php print $label ?>:
      </td>
      <td>
        <?php if ($items): ?>
        <table>
        <?php foreach ($items as $delta => $item): ?>
          <?php foreach (element_children($item['node'], FALSE) as $key): ?>
          <?php $node = $item['node'][$key]; ?>
          <tr>
            <?php if (isset($node['field_number'])): ?>
            <td class="number">
              <a href="<?php print url('node/' . $node['#node']->nid . '/pdf', array('absolute' => TRUE)); ?>"><?php print render($node['field_number']); ?></a>
            </td>
            <td class="policy-title">
            <?php if (isset($node['field_form_title'])): ?>
              <?php print render($node['field_form_title']); ?>
            <?php elseif (isset($node['field_policy_title'])):?>
              <?php print render($node['field_policy_title']); ?>
            <?php elseif (isset($node['field_procedure_title'])): ?>
              <?php print render($node['field_procedure_title']); ?>
            <?php elseif (isset($node['field_standard_title'])): ?>
              <?php print render($node['field_standard_title']); ?>
            <?php elseif (isset($node['field_task_title'])): ?>
              <?php print render($node['field_task_title']); ?>
            <?php endif; ?>
            </td>
            <?php else: ?>
            <td colspan="2" class="see-also-link">
            <?php if (isset($node['field_link'])): ?>
            <?php print render($node['field_link']); ?>
            <?php else: ?>
            <?php print render($node['#node']->title); ?>
            <?php endif; ?>
            </td>
            <?php endif; ?>
          </tr>
          <?php endforeach; ?>
        <?php endforeach; ?>
        </table>
        <?php endif; ?>
      </td>
    </tr>
  </table>

</div>
