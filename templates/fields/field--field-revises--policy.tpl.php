<?php

/**
 * @file
 * Template override for revises field on policy content type.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <table>
    <tr>
      <td class="field-label"<?php print $title_attributes; ?>>
        <?php print $label ?>:
      </td>
      <td>
        <table>
          <?php foreach ($items as $delta => $item): ?>
          <tr><?php print render($item); ?></tr>
          <?php endforeach; ?>
        </table>
      </td>
    </tr>
  </table>
</div>
