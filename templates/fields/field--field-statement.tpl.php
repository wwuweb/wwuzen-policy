<?php

/**
 * @file
 * Template override for statement field.
 */
?>
<div class="statement"<?php print $attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
  <?php endforeach; ?>
</div>
