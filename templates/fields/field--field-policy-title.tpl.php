<?php

/**
 * @file
 * Template override for number field.
 */
?>
<span class="policy-title"<?php print $attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
  <?php endforeach; ?>
</span>
