<?php

/**
 * @file
 * Template override for definition field.
 */
?>
<div class="definition"<?php print $attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
  <?php endforeach; ?>
</div>
