<?php

/**
 * @file
 * Template override for field defintitions on policy content type.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <h3 class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:</h3>
  <?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
  <?php endforeach; ?>
</div>
