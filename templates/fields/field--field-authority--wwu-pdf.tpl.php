<?php

/**
 * @file
 * Template override for authority field.
 */
?>
<table class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <tr>
    <td class="field-label"<?php print $title_attributes; ?>>
      <?php print $label ?>:
    </td>
    <td class="field-value">
      <?php foreach ($items as $delta => $item): ?>

      <?php if ($delta == count($items) - 1): ?>
      <span class="authority-<?php print $delta; ?> last"><?php print render($item); ?></span>
      <?php elseif ($delta == 0): ?>
      <span class="authority-<?php print $delta; ?> first"><?php print str_replace(array("\r", "\n"), '', render($item) . ','); ?></span>
      <?php else: ?>
      <span class="authority-<?php print $delta; ?>"><?php print str_replace(array("\r", "\n"), '', render($item) . ','); ?></span>
      <?php endif; ?>

      <?php endforeach; ?>
    </td>
  </tr>
</table>
