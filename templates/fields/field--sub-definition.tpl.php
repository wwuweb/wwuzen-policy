<?php

/**
 * @file
 * Template override for field defintitions on policy content type.
 */
?>
<ul class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
  <li><?php print render($item); ?></li>
  <?php endforeach; ?>
</ul>
