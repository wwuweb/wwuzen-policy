<?php

/**
 * @file
 * Template override for description field.
 */
?>
<div class="description"<?php print $attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
  <?php endforeach; ?>
</div>
