<?php

/**
 * @file
 * Template override for field link on authority content type.
 */
foreach ($items as $delta => $item) {
  print render($item);
}
