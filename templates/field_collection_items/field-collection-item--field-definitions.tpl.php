<?php

/**
 * @file
 * Template override for definition field collection item.
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print render($content['field_definition']); ?>
</div>
