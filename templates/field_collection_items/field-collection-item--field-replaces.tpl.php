<?php

/**
 * @file
 * Template override for statements field collection item.
 */
?>
<td class="number"><?php print render($content['field_number']); ?></td>
<td class="policy-title"><?php print render($content['field_policy_title']); ?></td>
