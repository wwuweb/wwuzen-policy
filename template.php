<?php

/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 */

/**
 * Implements template_preprocess_node().
 */
function wwuzen_policy_preprocess_node(&$variables) {
  $node = $variables['node'];
  $view_mode = $variables['view_mode'];

  if ($view_mode !== 'full') {
    $variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $view_mode;
  }
}

/**
 * Implements template_preprocess_entity().
 */
function wwuzen_policy_preprocess_entity(&$variables, $hook) {
  $bundle = $variables['elements']['#bundle'];

  switch ($bundle) {
    case 'field_definitions':
      $variables['classes_array'] = array();
      $variables['classes_array'][] = 'definition-item';

      break;

    case 'field_statements':
      $variables['classes_array'] = array();
      $variables['classes_array'][] = 'statement-item';

      break;

  }
}

/**
 * Implements template_preprocess_field().
 */
function wwuzen_policy_preprocess_field(&$variables, $hook) {
  $element = $variables['element'];
  $field_name = $element['#field_name'];
  $view_mode = $element['#view_mode'];

  if ($view_mode !== 'full') {
    $variables['theme_hook_suggestions'][] = 'field__' . $field_name . '__' . $view_mode;
  }

  if ($field_name === 'field_number' && module_exists('wwu_policies')) {
    foreach (element_children($variables['items'], FALSE) as $key) {
      $variables['items'][$key]['#prefix'] = wwu_policies_get_prefix($element['#bundle']);
    }
  }

  if ($element['#bundle'] === 'policy') {
    $variables['classes_array'] = array();
    $variables['classes_array'][] = str_replace('field-', '', $variables['field_name_css']);
  }
}
